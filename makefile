BUILD_DIR=build
EXECUTABLE=sudoku_solver
WINDOWS=$(BUILD_DIR)/$(EXECUTABLE)_windows_amd64.exe
LINUX=$(BUILD_DIR)/$(EXECUTABLE)_linux_amd64
DARWIN=$(BUILD_DIR)/$(EXECUTABLE)_darwin_amd64

.PHONY: default all test clean

default: all


windows: $(WINDOWS) ## Build for Windows

linux: $(LINUX) ## Build for Linux

darwin: $(DARWIN) ## Build for Darwin (macOS)

$(WINDOWS):
	env GOOS=windows GOARCH=amd64 go build -i -v -o $(WINDOWS) -ldflags="-s -w -X main.version=$(VERSION)"  ./cmd/main.go

$(LINUX):
	env GOOS=linux GOARCH=amd64 go build -i -v -o $(LINUX) -ldflags="-s -w -X main.version=$(VERSION)"  ./cmd/main.go

$(DARWIN):
	env GOOS=darwin GOARCH=amd64 go build -i -v -o $(DARWIN) -ldflags="-s -w -X main.version=$(VERSION)"  ./cmd/main.go


build: windows linux darwin ## Build binaries

all: build ## Build


clean: ## Remove previous build
	rm -f $(WINDOWS) $(LINUX) $(DARWIN)



help: ## Display available commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
