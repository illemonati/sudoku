package main

import (
	"gitlab.com/illemonati/sudoku"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	input = kingpin.Arg("grid", "grid as a single string, with 0 repersenting blank cells").Required().String()
)

func main() {
	kingpin.Parse()
	grid, err := sudoku.StringToGrid(*input)
	if err != nil {
		println("Your input is not a sudoku grid")
		return
	}
	println("Original")
	sudoku.PrintGrid(grid)
	println("Solved")
	solved, err := sudoku.SolveSudoku(grid)
	if err != nil {
		println("Your input grid is unsolvable")
	}
	sudoku.PrintGrid(solved)
}
