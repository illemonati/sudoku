package sudoku

import (
	"errors"
	"strconv"
)

type SudokuCell struct {
	number    int
	needSolve bool
}

//SudokuGrid : Grid
type SudokuGrid [9][9]SudokuCell

//SolveSudoku : Solves a grid
func SolveSudoku(grid SudokuGrid) (SudokuGrid, error) {
	n := 0
	backtracking := false
	for {
		row, col := numberToRowCol(n)
		// PrintGrid(grid)
		// println(row, " ", col, "", n)
		if n > 80 {
			break
		}
		if n < 0 {
			return grid, errors.New("Invalid Grid")
		}

		if backtracking {
			if !(grid[row][col].needSolve) {
				n--
				continue
			}
			for i := grid[row][col].number + 1; i < 11; i++ {
				if i == 10 {
					grid[row][col].number = 0
					n--
					break
				}
				grid[row][col].number = i
				if cellWorks(grid, row, col) {
					backtracking = false
					n++
					break
				}
			}

		} else {
			if !(grid[row][col].needSolve) {
				n++
				continue
			}
			for i := 1; i < 11; i++ {
				if i == 10 {
					backtracking = true
					grid[row][col].number = 0
					n--
					break
				}
				grid[row][col].number = i
				// print(grid[row][col])
				// println(i, " ", cellWorks(grid, row, col))
				if cellWorks(grid, row, col) {
					n++
					break
				}
			}
		}
	}
	return grid, nil
}

func numberToRowCol(n int) (row, col int) {
	row = int(n / 9)
	col = int(n % 9)
	return
}

//StringToGrid : convert string to SudokuGrid
func StringToGrid(gridstring string) (res SudokuGrid, err error) {
	for i := 0; i < 81; i += 9 {
		row := gridstring[i : i+9]
		for j, col := range row {
			num, err := strconv.Atoi(string(col))
			if err != nil {
				return res, err
			}
			if num == 0 {
				res[i/9][j] = SudokuCell{num, true}
			} else {
				res[i/9][j] = SudokuCell{num, false}
			}
		}
	}
	return
}

func cellWorks(grid SudokuGrid, row, col int) bool {
	// print(columnRepeat(grid, row, col), " ", rowRepeat(grid, row, col), " ", boxRepeat(grid, row, col))
	return !(columnRepeat(grid, row, col) || rowRepeat(grid, row, col) || boxRepeat(grid, row, col)) && (grid[row][col].number != 0)

}

func columnRepeat(grid SudokuGrid, row, col int) bool {
	for i := 0; i < 9; i++ {
		if i == row {
			continue
		}
		if grid[i][col].number == grid[row][col].number {
			// print("--", grid[i][col].number, "=", grid[row][col].number, "--")
			return true
		}
	}
	return false
}

func rowRepeat(grid SudokuGrid, row, col int) bool {
	for i := 0; i < 9; i++ {
		if i == col {
			continue
		}
		if grid[row][i].number == grid[row][col].number {
			return true
		}
	}
	return false
}

func boxRepeat(grid SudokuGrid, row, col int) bool {
	startingRow, startingCol := getBoxLeftUpCell(getBoxNumber(row, col))
	for i := startingRow; i < startingRow+3; i++ {
		for j := startingCol; j < startingCol+3; j++ {
			if i == row || j == col {
				continue
			}
			if grid[i][j].number == grid[row][col].number {
				return true
			}
		}
	}
	return false
}

func getBoxLeftUpCell(boxNumber int) (row, col int) {
	row = int(boxNumber/3) * 3
	col = (boxNumber % 3) * 3
	return
}

func getBoxNumber(row, col int) (box int) {
	if row == 0 || row == 1 || row == 2 {
		if col == 0 || col == 1 || col == 2 {
			box = 0
		} else if col == 3 || col == 4 || col == 5 {
			box = 1
		} else {
			box = 2
		}
	} else if row == 3 || row == 4 || row == 5 {
		if col == 0 || col == 1 || col == 2 {
			box = 3
		} else if col == 3 || col == 4 || col == 5 {
			box = 4
		} else {
			box = 5
		}
	} else if row == 6 || row == 7 || row == 8 {
		if col == 0 || col == 1 || col == 2 {
			box = 6
		} else if col == 3 || col == 4 || col == 5 {
			box = 7
		} else {
			box = 8
		}
	}
	return
}

//PrintGrid : Print a SudokuGrid
func PrintGrid(grid SudokuGrid) {
	println("_______________________________")
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			print(grid[i][j].number, " ")
		}
		println()
	}
	println("_______________________________")
}
